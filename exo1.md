Instructions
============

-   Tous les TP Git doivent être faits principalement depuis le terminal
    (*Git Bash* sous Windows).
-   Suggestion d'organisation :
    -   À la première ouverture du terminal, créer un dossier `tpgit`
        avec `mkdir tpgit`
    -   Se placer dedans avec `cd tpgit`
    -   Ce dossier sera votre emplacement de base pour ce cours,
        n'hésitez pas à créer des sous-dossiers chaque fois que c'est
        pertinent !
    -   En particulier, on n'entrera pas de commande `git init` dans ce
        dossier, mais seulement dans un sous-dossier.

1.  Créer un répertoire `depot_test`, se déplacer dedans et initialiser
    un dépôt Git en tapant `git init`. *Vérifier qu'un sous-dossier
    `.git` est apparu : c'est là que Git enregistre tout ce qui le
    concerne dans ce dépôt.*
2.  Créer un fichier texte `bonjour.txt` et lui ajouter du contenu, par
    exemple :
```
Hello world !
Ceci est le dépôt de [votre nom].
```

**NB** : si j'utilise des crochets pour « encadrer » une expression,
cela signifie que toute l'expression encadrée est à remplacer (y compris
les crochets !).

3.  Le fichier est actuellement *non suivi*. Vérifier que c'est bien ce
    qui est indiqué en tapant `git status`.
4.  Ajouter le fichier `bonjour.txt` à la zone d'index avec
    `git add bonjour.txt`. *Taper `git status` à nouveau et
    remarquer la différence.*
5.  Valider votre premier commit avec `git commit` :
    -   Un éditeur s'ouvre avec un fichier temporaire prérempli
    -   Écrire un message sur la première ligne : « Premier commit »
    -   Enregistrer et quitter
6.  Afficher l'historique en tapant `git log`.
7.  Modifier la première ligne de `bonjour.txt` à votre guise.
8.  Créer un second fichier `README.md` avec le contenu suivant :
```
# Mon premier dépôt Git Ceci est mon premier dépôt.

## Liste des commandes
- `git init` : initialise le dépôt
- `git add` : ajoute un fichier à la zone d'index
- `git commit` : valide les modifications indexées dans la zone d'index
```

9.  Utiliser la commande `git status` pour voir l'état du dépôt. *Vous
    devriez avoir un fichier modifié (`bonjour.txt`) et un fichier non
    suivi (`README.md`).*
10. Indexer ces deux fichiers en utilisant la commande `git add`.
11. Valider votre second commit avec `git commit`, puis afficher
    l'historique à nouveau (`git log`).
12. En modifiant le fichier `README.md`, ajouter à la liste des
    commandes celles qui manquent (`git status` et `git log`). *Veiller
    à utiliser des « copier-coller » pour respecter la syntaxe utilisée
    pour le début de la liste...*
13. Enfin, entrer la commande
    `git commit -am "Ajoute des commandes à la liste"`.

Remarquer que vous n'avez rien à écrire de plus : les fichiers
*modifiés* sont automatiquement indexés et validés du même coup.

Explications de la commande :

-   L'option `-a` ajoute au commit les fichiers *modifiés* (sans qu'ils
    n'aient été *indexés*). Par contre, Git ignorera toujours les
    fichiers *non suivis*.
-   L'option `-m` permet d'ajouter le message de commit directement en
    une seule commande, sans passer par l'éditeur temporaire.

Lors des prochains exercices, vous pourrez toujours mettre à jour la
liste des commandes que vous apprenez à utiliser.

Pour aller plus loin
====================

Si votre dernier commit ne vous convient pas, la solution préférable est
de modifier vos fichiers et de recréer un commit.

**Cependant**, si vous ne voulez pas avoir un commit « erreur » dans
votre historique, vous pouvez modifier vos fichiers, les indexer, puis
entrer `git commit --amend`. Ceci vous permettra de remplacer le dernier
commit, au lieu d'en créer un supplémentaire.

**Attention !** On verra par la suite comment synchroniser son dépôt
local (votre ordinateur) avec un dépôt distant (sur un serveur). Alors,
si votre but est d'avoir un bel historique, *n'utilisez pas `--amend`
après une synchronisation* !

