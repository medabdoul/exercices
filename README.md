# Exercices

Ceci est le répertoire des exercices accompagnant le [cours de système de gestion de version](https://www.normalesup.org/~duchamps/files/git_initiation/cours.html) du M1 de modélisation statistique de Besançon.

## Liste des exercices :

- [Exo 1](exo1.md) : création d'un premier dépôt local, premiers commits.
- [Exo 2](exo2.md) : manipulation de dépôts distants, _pull_ et _push_.
- [Exo 3](exo3.md) : un _fork_ et une _merge request_.
- [Exo 4](exo4.md) : gestion de branches, fusions.

