Clones et Forks
===============

1.  Créer un dépôt `Dépôt test n°2`, initialisé avec un fichier
    `README.md` sur votre compte GitLab, et retenir son *slug*.
2.  Dans votre répertoire de travail, taper
    `git clone git@gitlab.com:[votre login]/[slug du dépôt]`.

Note : cette commande clone le dépôt nouvellement créé dans un dossier
local du même nom. Pour cloner le dépôt dans un dossier avec un nom
spécifique `mon_depot`, on peut utiliser `git clone [URL] mon_depot`.

Quand un dépôt est cloné, une branche `master` est créée en local. Elle
suit la branche `master` du dépôt originel (qui est donc « en amont »).
Le nom du *remote* correspondant au dépôt originel est `origin` par
défaut.

3.  Se déplacer dans le dossier `depot-test-n-2` (vérifier la présence
    du fichier `README.md` dans ce dossier, sur votre ordinateur).
4.  Créer un fichier `log.txt` contenant le résultat de la commande
    `git log`.
5.  L'ajouter, faire un commit et pousser avec `git push`.

Remarque : on n'a pas besoin de préciser `git push origin master` car la
branche `master` du dépôt cloné suit automatiquement la branche `master`
du dépôt distant (le remote `origin`).

6.  Sur GitLab, vérifier que le dépôt a bien intégré les nouvelles
    modifications.
7.  Supprimer maintenant le dépôt en allant dans « Settings \> General
    », dans la section « Advanced », tout en bas → « Delete project ».

Remarque : c'est compliqué de supprimer un projet. GitLab cherche à
compliquer la tâche puisque alors on perd tout le travail effectué et
l'historique du projet. *À ne faire que si le projet ne contient pas de
travail (comme ici), sinon on risque de le regretter...*

8.  Dans votre répertoire de travail, taper
    `git clone git@gitlab.com:groupe-cours-git/exo-fork`.
9.  Créer un fichier nommé avec votre login GitLab et contenant votre
    adresse mail GitLab.
10. « Commit » ses changements, essayer de « push ». *NB : ça ne devrait
    pas marcher.*

Vous n'avez a priori pas de droits pour pousser sur mon dépôt ! Je
pourrais vous ajouter comme développeurs sur mon projet, mais on va
passer par le mécanisme des *merge requests*.

11. Depuis la page de mon projet [Exo
    Fork](https://gitlab.com/groupe-cours-git/exo-fork), cliquer sur
    *fork* pour copier le dépôt sur votre compte.
12. Depuis votre dépôt local, créer un *remote* nommé `macopie` qui
    pointe vers votre copie (distante). *Cf. l'exercice précédent pour
    la syntaxe...*
13. Taper `git push --set-upstream macopie master`. *Rappel* : ceci va
    *pousser* vers le nouveau remote, et faire en sorte que la branche
    master locale *suive* la branche master du remote.
14. Depuis GitLab, faire une nouvelle *merge request* de votre branche
    vers mon dépôt :

« Merge requests » → « New merge request », sélectionner *votre* branche
`master` comme source, *ma* branche `master` comme destination. Valider
avec « Create merge request ».

15. Après que votre *merge requests* ait été validée, vous vérifierez
    que mon dépôt est mis à jour en *pullant* : `git pull origin`.

**Dans cet exercice**, on a vu que :

-   Si l'on crée un dépôt distant sur son compte GitLab, on peut
    simplement le *cloner* en local, et travailler avec des simples
    *pull / push*.
-   Si l'on veut *pousser* vers un dépôt qui ne nous appartient pas, il
    faut passer par une *merge request*:
    -   D'abord dupliquer (*fork*) le dépôt sur son compte.
    -   Le cloner en local, faire ses modifications.
    -   Pousser vers son *fork*.
    -   Depuis GitLab, proposer la *merge request*.

Pour aller plus loin
--------------------

Avec GitLab, on peut créer directement un dépôt distant qui copie le
dépôt local (utile pour automatiser la création de nombreux dépôts à
partir de votre machine) :

    git remote add origin git@gitlab.com:[votre login]/nouveau_depot
    git push --set-upstream origin master

Ceci crée un nouveau dépôt `nouveau_depot` sur votre compte, si celui-ci
n'existe pas !
