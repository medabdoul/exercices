Dépôts distants
===============

1.  Créer un dépôt (*project/repository*) `Dépôt test distant` sur votre
    compte GitLab. Créer un dépôt vierge, public, **sans** « initialiser
    le dépôt avec un fichier README ». Retenir l'identifiant / *slug*
    (« *Project slug* ») du dépôt distant créé, normalement
    `depot-test-distant`.

On va remplir ce dépôt avec l'historique du dépôt local créé lors du
premier exercice :

2.  Depuis le terminal, aller dans votre répertoire `depot_test`.
3.  Taper
    `git remote add mon_depot_distant git@gitlab.com:[votre login]/[slug du dépôt]`
    pour ajouter le dépôt GitLab nouvellement créé comme *remote* avec
    le nom `mon_depot_distant`.
4.  Taper `git push mon_depot_distant master` et vérifier l'état du
    dépôt sur GitLab. Cette commande « pousse » (copie) la branche
    locale actuelle sur la branche `master` du *remote*
    `mon_depot_distant`.

**NB** : une branche est un point de repère dans l'historique.
Contrairement à un *tag* (étiquette), une branche évolue au fil des
commit que l'on fait : si l'on est sur la branche `master` et que l'on
rajoute un nouveau commit, la branche `master` désignera ce dernier
commit.

Remarquer que le fichier `README.md` s'affiche en présentation du dépôt,
formaté proprement. Pour plus de détails sur la syntaxe `Markdown`, cf :

-   <https://docs.roadiz.io/fr/latest/user/write-in-markdown/> pour une
    entrée en matière
-   <https://daringfireball.net/projects/markdown/syntax> pour la
    syntaxe `Markdown` originale
-   <https://docs.gitlab.com/ee/user/markdown.html> pour le
    *GitLab-flavored Markdown*, version étendue de Markdown (en fait
    basée sur le *GitHub-flavored Markdown*)

5.  Taper `git branch master --set-upstream-to=mon_depot_distant/master`
    pour que la branche *locale* `master` suive automatiquement la
    branche `master` du *remote* `mon_depot_distant`.

**NB** : *upstream* se traduit comme « en amont » (pour un cours d'eau),
ici ça permet de distinguer une branche distante comme étant
particulière. Elle est désignée comme la « source » de la branche
locale, au sens ou quand on va récupérer ou ajouter les dernières
modifications en ligne, cela se fera à partir de cette branche « en
amont ».

6.  Ceci permet maintenant d'utiliser `git push` à la place de
    `git push mon_depot_distant master`. En local, modifier le fichier
    `README.md` (*par exemple, rajouter les commandes apprises à la
    liste préexistante*), faire un commit et tester la commande
    `git push`.

Note : on peut à la fois « pousser » la branche locale et lui faire
suivre une branche distante `master` pour toutes les prochaines fois en
une seule commande :

    git push mon_depot_distant master --set-upstream

7.  Modifiez à nouveau `README.md` comme vous le voulez, puis faites un
    commit.
8.  Utiliser la commande `git status` pour vérifier que la branche
    distante est « en retard d'un commit ». Sur la page du dépôt
    distant, aller voir l'historique et vérifier qu'en effet ce dernier
    commit n'y apparaît pas.
9.  Pousser ce nouveau commit et vérifier que le dépôt distant est
    modifié.
10. *Depuis la page du projet sur GitLab*, modifier le fichier
    `README.md` (avec le « WebIDE » fourni par GitLab) en ajoutant la
    ligne suivante :
```
- `git clone [lien SSH ou HTTPS]` : permet de cloner un dépôt distant en local
```

**NB** : Il faudra valider le commit après la modification du fichier en ligne.

11. En local, vérifier cette fois (avec `git log`) que les changement ne
    sont pas pris en compte sur votre ordinateur. Taper `git pull` pour
    récupérer ce nouveau commit.

**Retenir** : *push* permet de pousser (des commits) vers un dépôt
distant, *pull* permet de tirer (des commits) vers le dépôt local.

Dans cet exercice, on a vu comment :

-   Ajouter un dépôt distant (ou *remote*)
-   Pousser (*push*) des commits vers une branche distante
-   Tirer (*pull*) des commits vers sa branche locale
-   Spécifier une branche distante comme « en amont » (ou *upstream*)
    d'une branche locale, pour simplifier l'utilisation des commandes
    *pull / push*.

Dans le prochain exercice, on verra que toute la partie « création d'un
dépôt local, ajout d'un dépôt distant, suivi d'une branche en amont »
est en général condensée par une seule commande : `git clone`.

À faire s'il reste du temps
===========================

12. Créer une étiquette `v0` sur le premier commit du dépôt avec
    `git tag v0 [SHA-1 du commit]`. Rappel : *on trouve le SHA-1 d'un
    commit avec `git log`.*
13. Créer une étiquette `v1` sur le commit actuel (pas besoin de SHA-1
    ici...).

Par défaut, les étiquettes ne sont pas transférées lors d'un push.

14. Transférer vos tags avec `git push --tags`.
