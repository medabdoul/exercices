Branches
========

1.  Cloner le dépôt [Exo
    Branches](https://gitlab.com/groupe-cours-git/exo-branches) dans un
    répertoire de travail.

**NB** : Si vous avez bien fait les exercices précédents, vous avez le
rôle de « Développeur » sur le groupe « Groupe cours Git » sur GitLab,
et donc vous avez maintenant le droit de *pousser* vers les dépôts du
groupe.

Ceci implique que **vos camarades travaillent tous·tes simultanément**
sur le même dépôt : avant de pousser vos commits, pensez à taper
`git pull` afin de mettre à jour votre historique.

2.  Taper `git checkout dev` pour basculer sur la branche `dev` (cette
    branche existe sur le dépôt distant, elle a donc été aussi créée en
    local).
3.  Créer un fichier nommé avec votre login GitLab, contenant la phrase
```
Je manipule la branche `dev`
```

4.  Commit ce nouveau fichier, pousser le commit.
5.  Vérifier que la branche `master` n'est pas modifiée en basculant
    dessus (*utiliser à nouveau la commande `git checkout`*). *En
    particulier, le fichier créé à l'étape précédente doit avoir disparu
    !*
6.  Taper `git checkout -b dev_[votre login]`. Ceci va créer une
    nouvelle branche `dev_[votre login]` et vous faire basculer dessus.
7.  Répéter l'étape 3 en adaptant la phrase (comme vous voulez).
    *Attention, pour cet exercice, le nom du fichier créé sur la branche
    `dev` doit être **le même** que celui créé sur la branche
    `dev_[votre login]`*.
8.  Taper `git diff dev` pour examiner les différences entre les
    branches `dev` et `dev_[votre login]`.
9.  Basculer sur la branche `master`, et fusionner la branche `dev` avec
    `git merge dev` (c'est une fusion fast-forward).
10. Tenter de fusionner la branche `dev_[votre_login]` dans la branche
    `master`. *Il devrait y avoir un conflit, que l'on va maintenant
    résoudre.*
11. Résoudre le conflit en modifiant le contenu du fichier posant
    problème. Par exemple, remplacer son contenu par :
```
J'ai manipulé les branches `dev` et `dev_[votre login]`,
fusionné ces deux branches dans la branche `master`,
et résolu un conflit.

Git c'est trop simple !
```

12. Bien sûr, pour résoudre complètement le conflit il faut le signaler
    : *ajouter* le fichier à la zone d'index et *valider* le changement
    par un commit.
13. Pousser la branche `master` sur le dépôt distant. (*Encore une fois,
    il faudra d'abord tirer des commits si vos camarades ont poussé les
    leurs avant vous !*)
